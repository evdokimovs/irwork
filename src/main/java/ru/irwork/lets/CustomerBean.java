package ru.irwork.lets;

import java.io.Serializable;
import java.util.*;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import org.apache.log4j.Logger;
import org.apache.log4j.Priority;
import ru.irwork.lets.model.OrderExtModel;

/**
 *
 * @author irwork
 */
@ManagedBean
@SessionScoped
public class CustomerBean implements Serializable {
    private static final Logger LOG = Logger.getLogger(LetsBuyApp.class);
    private static final long serialVersionUID = 1L;
    private String customer = "";
    private List<String> customers;
    private List<OrderExtModel> orders;

    @ManagedProperty(value = "#{letsBuyApp}")
    private LetsBuyApp app;
    //This method is required for dependency injection (ManagedProperty)

    public void setApp(LetsBuyApp appBean) {
        this.app = appBean;
    }

    public List<String> getCustomers() {
        if (customers == null || customers.isEmpty()) {
            customers = app.getAllCustomerNames();
        }
        return customers;
    }

    public void setCustomers(List<String> customers) {
        this.customers = customers;
    }

    public String getCustomer() {
        return customer;
    }

    public void setCustomer(String customer) {
        this.customer = customer;
    }

    public void showCustomerInfo() {
        LOG.log(Priority.DEBUG, "showCustomerInfo()");
        if (getCustomer() == null || getCustomer().length() == 0) {
            return;
        }
        orders = null;
    }

    public List<OrderExtModel> getOrders() {
        if (getCustomer() == null || getCustomer().length() == 0) {
            orders = null;
        } else {
            orders = app.getOrderExtsBy(customer);
        }
        return orders;
    }

    public void setOrders(List<OrderExtModel> orders) {
        this.orders = orders;
    }

    public void deleteOrder(int orderId) {
        //resultMessage = new FacesMessage("Удаление заказа:"   "Действие не выполнено.")
        FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "\u0423\u0434\u0430\u043b\u0435\u043d\u0438\u0435 \u0437\u0430\u043a\u0430\u0437\u0430:", "\u0414\u0435\u0439\u0441\u0442\u0432\u0438\u0435 \u043d\u0435 \u0432\u044b\u043f\u043e\u043b\u043d\u0435\u043d\u043e.");
        boolean updated = app.deleteOrderBy(orderId);
        if (updated) {
            this.orders = app.getOrderExtsBy(customer);
            //resultMessage = new FacesMessage("Удаление заказа:"   "Действие выполнено успешно.")
            msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "\u0423\u0434\u0430\u043b\u0435\u043d\u0438\u0435 \u0437\u0430\u043a\u0430\u0437\u0430:", "\u0414\u0435\u0439\u0441\u0442\u0432\u0438\u0435 \u0432\u044b\u043f\u043e\u043b\u043d\u0435\u043d\u043e \u0443\u0441\u043f\u0435\u0448\u043d\u043e.");
        }
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    public void deleteItem(int itemId) {
        //resultMessage = new FacesMessage("Удаление пункта заказа:"   "Действие не выполнено.")
        FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "\u0423\u0434\u0430\u043b\u0435\u043d\u0438\u0435 \u043f\u0443\u043d\u043a\u0442\u0430 \u0437\u0430\u043a\u0430\u0437\u0430:", "\u0414\u0435\u0439\u0441\u0442\u0432\u0438\u0435 \u043d\u0435 \u0432\u044b\u043f\u043e\u043b\u043d\u0435\u043d\u043e.");
        boolean updated = app.deleteItemBy(itemId);
        if (updated) {
            this.orders = app.getOrderExtsBy(customer);
            //resultMessage = new FacesMessage("Удаление пункта заказа:"   "Действие выполнено успешно.")
            msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "\u0423\u0434\u0430\u043b\u0435\u043d\u0438\u0435 \u043f\u0443\u043d\u043a\u0442\u0430 \u0437\u0430\u043a\u0430\u0437\u0430:", "\u0414\u0435\u0439\u0441\u0442\u0432\u0438\u0435 \u0432\u044b\u043f\u043e\u043b\u043d\u0435\u043d\u043e \u0443\u0441\u043f\u0435\u0448\u043d\u043e.");
        }
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

}
