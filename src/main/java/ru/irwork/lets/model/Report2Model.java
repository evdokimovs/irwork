package ru.irwork.lets.model;

import java.io.Serializable;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

/**
 *
 * @author irwork
 */
@ManagedBean
@SessionScoped
public class Report2Model  implements Serializable {

    private static final long serialVersionUID = 1L;

    private int id;
    private String nick = "";
    private int weavingCutCount = 0;
    private double weavingCutLength = 0.;
    private double amount = 0.;
    private String info;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

    public double getWeavingCutCount() {
        return weavingCutCount;
    }

    public void setWeavingCutCount(int weavingCutCount) {
        this.weavingCutCount = weavingCutCount;
    }

    public double getWeavingCutLength() {
        return weavingCutLength;
    }

    public void setWeavingCutLength(double weavingCutLength) {
        this.weavingCutLength = weavingCutLength;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }
    
    
}
