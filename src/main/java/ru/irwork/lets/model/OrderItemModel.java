package ru.irwork.lets.model;

import java.io.Serializable;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

/**
 *
 * @author irwork
 */
@ManagedBean
@SessionScoped
public class OrderItemModel implements Serializable {

    private static final long serialVersionUID = 1L;
    private int id;
    private int fabricId;
    private String code = "";
    private int orderId;
    private double length;
    private boolean waitingList = false;
    private String nick = "";   // вспомогательное при выводе

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getFabricId() {
        return fabricId;
    }

    public void setFabricId(int fabricId) {
        this.fabricId = fabricId;
    }

    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    public double getLength() {
        return length;
    }

    public void setLength(double length) {
        this.length = length;
    }

    public boolean isWaitingList() {
        return waitingList;
    }

    public void setWaitingList(boolean waitingList) {
        this.waitingList = waitingList;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

    @Override
    public String toString() {
        return "OrderItemModel{" + "id=" + id + ", fabricId=" + fabricId + ", code=" + code + ", orderId=" + orderId + ", length=" + length + ", waitingList=" + waitingList + ", nick=" + nick + '}';
    }

}
