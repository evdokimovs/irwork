package ru.irwork.lets.model;

import java.io.Serializable;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

/**
 *
 * @author irwork
 */
@ManagedBean
@SessionScoped
public class FabricModel implements Serializable {

    private static final long serialVersionUID = 1L;
    private String code = "";
    private int rollNumber = 1;
    private double rollLength = 0.;
    private double price = 0.;
    private String imagePath = null;
    private String imageFolder = null;
    private String imageTitle = null;
    private int id;
    private int purchaseId;
    private boolean visible = false;
    private boolean approved = false;
    private double remainder = 0.; // для расчета, остаток от rollLengh
    private double bougthLength = 0.; // вспомогательное поле
    private double needLength = 0.; // вспомогательное поле
    private List<OrderExtModel> customers = null;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public int getRollNumber() {
        return rollNumber;
    }

    public void setRollNumber(int rollNumber) {
        this.rollNumber = rollNumber;
    }

    public double getRollLength() {
        return rollLength;
    }

    public void setRollLength(double rollLengh) {
        this.rollLength = rollLengh;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getPurchaseId() {
        return purchaseId;
    }

    public void setPurchaseId(int purchaseId) {
        this.purchaseId = purchaseId;
    }

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    public boolean isApproved() {
        return approved;
    }

    public void setApproved(boolean approved) {
        this.approved = approved;
    }

    public double getRemainder() {
        return rollLength - bougthLength;
    }

    public void setRemainder(double remainder) {
        this.remainder = remainder;
    }

    public double getBougthLength() {
        return bougthLength;
    }

    public void setBougthLength(double bougthLength) {
        this.bougthLength = bougthLength;
    }

    public double getNeedLength() {
        return needLength;
    }

    public void setNeedLength(double needLength) {
        this.needLength = needLength;
    }

    public List<OrderExtModel> getCustomers() {
        return customers;
    }

    public void setCustomers(List<OrderExtModel> customers) {
        this.customers = customers;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public String getImageFolder() {
        return imageFolder;
    }

    public void setImageFolder(String imageFolder) {
        this.imageFolder = imageFolder;
    }

    public String getImageTitle() {
        return imageTitle;
    }

    public void setImageTitle(String imageTitle) {
        this.imageTitle = imageTitle;
    }

}
