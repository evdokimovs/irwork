package ru.irwork.lets.model;

import java.io.Serializable;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

/**
 *
 * @author irwork
 */
@ManagedBean
@SessionScoped
public class Report1Model implements Serializable {

    private static final long serialVersionUID = 1L;

    private int id;
    private String code = "";
    private int rollNumber = 1;
    private double rollLength = 0.;
    private double bougthLength = 0.;
    private int level;

    private String info;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public int getRollNumber() {
        return rollNumber;
    }

    public void setRollNumber(int rollNumber) {
        this.rollNumber = rollNumber;
    }

    public double getRollLength() {
        return rollLength;
    }

    public void setRollLength(double rollLength) {
        this.rollLength = rollLength;
    }

    public double getBougthLength() {
        return bougthLength;
    }

    public void setBougthLength(double bougthLength) {
        this.bougthLength = bougthLength;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

}
