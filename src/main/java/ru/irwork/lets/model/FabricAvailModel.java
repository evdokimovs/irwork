package ru.irwork.lets.model;

import java.io.Serializable;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

/**
 *
 * @author irwork
 */
@ManagedBean
@SessionScoped
public class FabricAvailModel implements Serializable {

    private static final long serialVersionUID = 1L;
    private String code = "";
    private double rollLength = 0.; // 
    private String imagePath = null;
    private String imageFolder = null;
    private String imageTitle = null;
    private double remainder = 0.; // для расчета, остаток от rollLengh
    private double totalLength = 0.; // вспомогательное поле

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public double getRollLength() {
        return rollLength;
    }

    public void setRollLength(double rollLength) {
        this.rollLength = rollLength;
    }

    public double getRemainder() {
        return remainder;
    }

    public void setRemainder(double remainder) {
        this.remainder = remainder;
    }

    public double getTotalLength() {
        return totalLength;
    }

    public void setTotalLength(double totalLength) {
        this.totalLength = totalLength;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public String getImageFolder() {
        return imageFolder;
    }

    public void setImageFolder(String imageFolder) {
        this.imageFolder = imageFolder;
    }

    public String getImageTitle() {
        return imageTitle;
    }

    public void setImageTitle(String imageTitle) {
        this.imageTitle = imageTitle;
    }

}
