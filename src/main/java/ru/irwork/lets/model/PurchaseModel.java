package ru.irwork.lets.model;

import java.io.Serializable;
import java.util.Date;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

/**
 *
 * @author irwork
 */
@ManagedBean
@SessionScoped
public class PurchaseModel implements Serializable {

    private static final long serialVersionUID = 1L;
    private String description = "None";
    private Date date;
    private int id = 0;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

}
