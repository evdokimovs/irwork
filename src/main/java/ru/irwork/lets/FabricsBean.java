package ru.irwork.lets;

import ru.irwork.lets.model.*;
import java.io.Serializable;
import java.util.*;

import java.util.regex.PatternSyntaxException;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import org.apache.log4j.Logger;
import org.primefaces.event.RowEditEvent;

/**
 *
 * @author irwork
 */
@ManagedBean
@SessionScoped
public class FabricsBean implements Serializable {

    private static final long serialVersionUID = 1L;
    private static final Logger LOG = Logger.getLogger(OrderBean.class);

    private List<FabricModel> fabricsAll;
    private String fabricsNew = "";
    double price = 0.;
    private List<FabricModel> selectedFabrics;

    @ManagedProperty(value = "#{letsBuyApp}")
    private LetsBuyApp app;
    //This method is required for dependency injection (ManagedProperty)

    public void setApp(LetsBuyApp appBean) {
        this.app = appBean;
    }

    public void addFabrics() {
        LOG.debug("begin addFabrics()");
        FacesMessage resultMessage = new FacesMessage("Nothing");
//        String outcome = "fabrics_input";
        LOG.debug("addFabrics: " + getFabricsNew());
        if (getFabricsNew() == null || getFabricsNew().length() == 0) {
            //resultMessage = new FacesMessage("Введите товар.");
            resultMessage = new FacesMessage("\u0412\u0432\u0435\u0434\u0438\u0442\u0435 \u0442\u043e\u0432\u0430\u0440.");
            FacesContext.getCurrentInstance().addMessage(null, resultMessage);
            return;
//            return outcome;
        }
        String str = getFabricsNew().replaceAll("\\s+", ""); // регулярные выражения \s - это "any whitespace symbol" , а + - это "один или более символ"
        str = str.replaceAll(",", "."); // регулярные выражения \s - это "any whitespace symbol" , а + - это "один или более символ"
        str = str.toUpperCase();

        if (str.length() == 0) {
            //resultMessage = new FacesMessage("Введите товар.");
            resultMessage = new FacesMessage("\u0412\u0432\u0435\u0434\u0438\u0442\u0435 \u0442\u043e\u0432\u0430\u0440.");
            FacesContext.getCurrentInstance().addMessage(null, resultMessage);
            return;
//            return outcome;
        }

        // парсим 
        List<FabricModel> rez = new ArrayList<>();
        String fabric = "";
        try {
            String[] subStr;
            String delimeter = ";"; // Разделитель
            subStr = str.split(delimeter);
            String del = "-"; // Разделитель
            String[] sub;
            for (String obj : subStr) {
                fabric = obj;
                if (obj.length() == 0) {
                    continue;
                }
                sub = obj.split(del);
                if (sub.length < 2 || sub.length > 3) {
                    throw new NumberFormatException();
                }
                FabricModel mod = new FabricModel();
                mod.setCode(sub[0]);
                if (sub.length == 2) {
                    mod.setRollLength(Double.parseDouble(sub[1]));
                } else {
                    mod.setRollNumber(Integer.parseInt(sub[1]));
                    mod.setRollLength(Double.parseDouble(sub[2]));
                }
                rez.add(mod);
            }

        } catch (PatternSyntaxException e) {
            //resultMessage = new FacesMessage("Некорректный формат ввода.");
            resultMessage = new FacesMessage("\u041d\u0435\u043a\u043e\u0440\u0440\u0435\u043a\u0442\u043d\u044b\u0439 \u0444\u043e\u0440\u043c\u0430\u0442 \u0432\u0432\u043e\u0434\u0430.");
            FacesContext.getCurrentInstance().addMessage(null, resultMessage);
            return;
//            return outcome;
        } catch (NumberFormatException e) {
            //resultMessage = new FacesMessage("Ошибка в написании : "+fabric);
            resultMessage = new FacesMessage("\u041e\u0448\u0438\u0431\u043a\u0430 \u0432 \u043d\u0430\u043f\u0438\u0441\u0430\u043d\u0438\u0438 : " + fabric);
            FacesContext.getCurrentInstance().addMessage(null, resultMessage);
            return;
//            return outcome;
        }

        if (rez.isEmpty()) {
            //resultMessage = new FacesMessage("Некорректный формат ввода.");
            resultMessage = new FacesMessage("\u041d\u0435\u043a\u043e\u0440\u0440\u0435\u043a\u0442\u043d\u044b\u0439 \u0444\u043e\u0440\u043c\u0430\u0442 \u0432\u0432\u043e\u0434\u0430.");
        } else {
            boolean added = rez.size() > 0 ? app.saveFabrics(rez) : false;
            if (added) {
                setFabricsNew("");
                //resultMessage = new FacesMessage("Заказ добавлен.");
                resultMessage = new FacesMessage(" \u0417\u0430\u043a\u0430\u0437 \u0434\u043e\u0431\u0430\u0432\u043b\u0435\u043d.");
            } else {
                //resultMessage = new FacesMessage("Не удалось сохранить.")
                resultMessage = new FacesMessage("\u041d\u0435 \u0443\u0434\u0430\u043b\u043e\u0441\u044c \u0441\u043e\u0445\u0440\u0430\u043d\u0438\u0442\u044c.");
            }
        }
        FacesContext.getCurrentInstance().addMessage(null, resultMessage);
        LOG.debug("end addFabrics()");
    }

    /*
    Действие по нажатию кнопки "Установить выбранным рулонам статус `Берем`"
    Источник - страница fabrics_editing
     */
    public void approved() {
        LOG.debug("begin approved()");
        //resultMessage = new FacesMessage("Внесение изменений:"   "Действие не выполнено.")
        FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "\u0412\u043d\u0435\u0441\u0435\u043d\u0438\u0435 \u0438\u0437\u043c\u0435\u043d\u0435\u043d\u0438\u0439:", "\u0414\u0435\u0439\u0441\u0442\u0432\u0438\u0435 \u043d\u0435 \u0432\u044b\u043f\u043e\u043b\u043d\u0435\u043d\u043e.");
        boolean updated = app.approvedFabrics();
        if (updated) {
            this.fabricsAll = app.getAllFabrics();
            //resultMessage = new FacesMessage("Внесение изменений:"   "Действие выполнено успешно.")
            msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "\u0412\u043d\u0435\u0441\u0435\u043d\u0438\u0435 \u0438\u0437\u043c\u0435\u043d\u0435\u043d\u0438\u0439:", "\u0414\u0435\u0439\u0441\u0442\u0432\u0438\u0435 \u0432\u044b\u043f\u043e\u043b\u043d\u0435\u043d\u043e \u0443\u0441\u043f\u0435\u0448\u043d\u043e.");
        }
        FacesContext.getCurrentInstance().addMessage(null, msg);
        LOG.debug("end approved()");
    }

    /*
    Действие по нажатию кнопки обновить. Предполагается обновление таблицы на странице fabrics_editing
    Источник - страница fabrics_editing
     */
    public void refreshFabrics() {
        LOG.debug("begin refreshFabrics()");
        refresh();
        //resultMessage = new FacesMessage("Обновлено :"   ")
        FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "\u041e\u0431\u043d\u043e\u0432\u043b\u0435\u043d\u043e :", "");
        FacesContext.getCurrentInstance().addMessage(null, msg);
        LOG.debug("end refreshFabrics()");
    }

    /*
    Обнуляет данные для страницы редактирование fabrics_editing
     */
    public void refresh() {
        this.fabricsAll = app.getAllFabrics();
        this.selectedFabrics = null;
        this.price = 0.;

    }

    /*
    Действие по нажатию кнопки "Опубликовать все ткани". 
    Источник - страница fabrics_editing
     */
    public void visibleAllFabrics() {
        LOG.debug("begin visibleAllFabrics()");
        //resultMessage = new FacesMessage("Сохранение :"   "Данные не сохранены.")
        FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "\u0421\u043e\u0445\u0440\u0430\u043d\u0435\u043d\u0438\u0435 :", "\u0414\u0430\u043d\u043d\u044b\u0435 \u043d\u0435 \u0441\u043e\u0445\u0440\u0430\u043d\u0435\u043d\u044b.");
        boolean updated = app.visibleAllFabrics(fabricsAll);
        if (updated) {
            fabricsAll = app.getAllFabrics();
            //resultMessage = new FacesMessage("Сохранение :"   "Данные сохранены успешно.")
            msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "\u0421\u043e\u0445\u0440\u0430\u043d\u0435\u043d\u0438\u0435 :", "\u0423\u0441\u043f\u0435\u0448\u043d\u043e\u0435 \u0437\u0430\u0432\u0435\u0440\u0448\u0435\u043d\u0438\u0435 \u043e\u043f\u0435\u0440\u0430\u0446\u0438\u0438.");
        }
        FacesContext.getCurrentInstance().addMessage(null, msg);
        LOG.debug("end visibleAllFabrics()");
    }

    public void onRowEdit(RowEditEvent event) {
        //resultMessage = new FacesMessage("Сохранение :"   "Данные не сохранены.")
        FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "\u0421\u043e\u0445\u0440\u0430\u043d\u0435\u043d\u0438\u0435 :", "\u0414\u0430\u043d\u043d\u044b\u0435 \u043d\u0435 \u0441\u043e\u0445\u0440\u0430\u043d\u0435\u043d\u044b.");
        FabricModel mod = (FabricModel) event.getObject();
        boolean updated = app.updateFabricModel(mod);
        if (updated) {
            //resultMessage = new FacesMessage("Сохранение :"   "Данные сохранены успешно.")
            msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "\u0421\u043e\u0445\u0440\u0430\u043d\u0435\u043d\u0438\u0435 :", "\u0423\u0441\u043f\u0435\u0448\u043d\u043e\u0435 \u0437\u0430\u0432\u0435\u0440\u0448\u0435\u043d\u0438\u0435 \u043e\u043f\u0435\u0440\u0430\u0446\u0438\u0438."
                    + ((FabricModel) event.getObject()).getCode());
        }
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    public void onRowCancel(RowEditEvent event) {
//        FabricModel mod = app.getFabricModelById(((FabricModel) event.getObject()).getId());

        FacesMessage msg = new FacesMessage("Edit Cancelled", ((FabricModel) event.getObject()).getCode());
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    public String getFabricsNew() {
        return fabricsNew;
    }

    public void setFabricsNew(String fabricsNew) {
        this.fabricsNew = fabricsNew;
    }

    public List<FabricModel> getFabricsAll() {
        if (fabricsAll == null) {
            fabricsAll = app.getAllFabrics();
        }
        return fabricsAll;
    }

    public void setFabricsAvail(List<FabricModel> fabricsAll) {
        this.fabricsAll = fabricsAll;
    }

    /*
    Действие по нажатию кнопки "Распределить поступление". 
    Источник - страница fabrics_editing
     */
    public void distributeNewFabrics() {
        app.distributionWaitingList(null);
        //resultMessage = new FacesMessage("Раздача :"   ")
        FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "\u0420\u0430\u0437\u0434\u0430\u0447\u0430 :", "\u0420\u0430\u0441\u043f\u0440\u0435\u0434\u0435\u043b\u0435\u043d\u0438\u0435 \u0442\u043a\u0430\u043d\u0435\u0439 \u0437\u0430\u0432\u0435\u0440\u0448\u0435\u043d\u043e");
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public List<FabricModel> getSelectedFabrics() {
        return selectedFabrics;
    }

    public void setSelectedFabrics(List<FabricModel> selectedFabrics) {
        this.selectedFabrics = selectedFabrics;
    }

    /*
    Назначение цены на группу товара selected
    Действие по нажатию кнопки "Назначить цену" -> "Сохранить". 
    Источник - страница fabrics_editing, диалоговое окно.
     */
    public void assignPrice() {
        LOG.debug("begin assignPrice()");
        //resultMessage = new FacesMessage("Сохранение :"   "Цена не назначена.")
        FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "\u0421\u043E\u0445\u0440\u0430\u043D\u0435\u043D\u0438\u0435 :", "\u0426\u0435\u043D\u0430\u0020\u043D\u0435\u0020\u043D\u0430\u0437\u043D\u0430\u0447\u0435\u043D\u0430.");
        if (selectedFabrics == null || selectedFabrics.isEmpty()) {
            //resultMessage = new FacesMessage("Действие:"   "Действие не выполнена. Товар не выбран.")
            msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "\u0414\u0435\u0439\u0441\u0442\u0432\u0438\u0435 :", "\u0414\u0435\u0439\u0441\u0442\u0432\u0438\u0435\u0020\u043D\u0435\u0020\u0432\u044B\u043F\u043E\u043B\u043D\u0435\u043D\u0430. \u0422\u043E\u0432\u0430\u0440\u0020\u043D\u0435\u0020\u0432\u044B\u0431\u0440\u0430\u043D.");
            FacesContext.getCurrentInstance().addMessage(null, msg);
            return;
        }
        boolean assigned = app.assignPrice(selectedFabrics, price);
        if (assigned) {
            //resultMessage = new FacesMessage("Сохранение :"   "Цена назначена успешно.")
            msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "\u0421\u043E\u0445\u0440\u0430\u043D\u0435\u043D\u0438\u0435 :", "\u0426\u0435\u043D\u0430\u0020\u043D\u0430\u0437\u043D\u0430\u0447\u0435\u043D\u0430\u0020\u0443\u0441\u043F\u0435\u0448\u043D\u043E."
            );
            refresh();
        }
        FacesContext.getCurrentInstance().addMessage(null, msg);
        LOG.debug("end assignPrice()");

    }

}
