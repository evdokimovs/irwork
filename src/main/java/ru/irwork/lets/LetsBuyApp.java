package ru.irwork.lets;

import ru.irwork.lets.model.*;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.*;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.model.SelectItem;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import org.apache.log4j.Logger;

/**
 *
 * @author irwork
 */
@ManagedBean
@ApplicationScoped
public class LetsBuyApp implements Serializable {

    private static final Logger LOG = Logger.getLogger(LetsBuyApp.class);
    private long lastPurchaseId = -1;
//    static{
//        try{
//            System.setProperty("ru.irwork.lets.level", String.valueOf(Level.INFO));
//            LogManager.getLogManager().readConfiguration();
//        }catch(Exception e){
//            System.err.println("Can't initialize logging for ru.irwork.lets");
//        }
//    }

    public Locale getLocale() {
        return new Locale("ru", "RU");
    }

    protected Connection getConnection() throws SQLException {
        try {
            Context ctx = new InitialContext();
            DataSource ds = (DataSource) ctx.lookup("java:/comp/env/jdbc/letsDB");
            return ds.getConnection();
        } catch (NamingException ex) {
            LOG.debug(ex, ex);
            throw new SQLException(ex);
        }
    }

    public long getLastPurchaseId() {
        if (lastPurchaseId < 0) {
            this.lastPurchaseId = getCurrentPurchase().getId();
        }
        return lastPurchaseId;
    }

    public void setLastPurchaseId(long id) {
        this.lastPurchaseId = id;
    }

    /*
    Сохранение заявки ползователя. 
    В лист ожидания записывается true. Затем запускается перерасчет distributionWaitingList(String nick)
     */
    public int saveOrder(String nick, String orderInfo, List<OrderExtModel> lst, String note) {
        LOG.debug("Begin saveOrder: " + orderInfo);
        int insertedRowsNumber = 0;
        boolean result = false;
        int orderId = 0;
        String str = "insert into Orders (purchase_id, nick, order_date, order_info, note) values ( ?, ?, CURRENT_TIMESTAMP(), ?, ? )";
        LOG.debug(str);
        try (Connection conn = getConnection(); PreparedStatement stat = conn.prepareStatement(str)) {
            stat.setLong(1, getLastPurchaseId());
            stat.setString(2, nick);
            stat.setString(3, orderInfo);
            stat.setString(4, note.length() == 0 ? null : note);
            result = stat.executeUpdate() > 0;
            if (!result) {
                return 0;
            }
            try (Statement st = conn.createStatement(); ResultSet rs = st.executeQuery("select last_insert_id();");) {
                if (rs != null && rs.next()) {
                    orderId = rs.getInt(1);
                }
            }
            String str3 = "insert into Order_items (fabric_code, order_id, length, waiting_list) values ( ?, ?, ?, ? )";
            LOG.debug(str3);
            try (PreparedStatement stat3 = conn.prepareStatement(str3)) {
                for (OrderExtModel mod : lst) {
                    stat3.setString(1, mod.getCode());
                    stat3.setLong(2, orderId);
                    stat3.setDouble(3, mod.getLength());
                    stat3.setBoolean(4, true);
                    insertedRowsNumber += stat3.executeUpdate();
                }
            }
        } catch (SQLException ex) {
            insertedRowsNumber = 0;
            LOG.error(str);
        }
        distributionWaitingList(nick);
        LOG.debug("End saveOrder, inserted rows " + insertedRowsNumber);
        return insertedRowsNumber;

    }

    /*
    Перерасчет листа ожидание заявок по нику пользователя
     */
    public int distributionWaitingList(String nick) {
        LOG.debug("Begin distributionWaitingList(), nick: " + nick);
        int updatedRows = 0;
        String st1 = "select * from Order_items oi, Orders o where oi.order_id = o.id and o.purchase_id = ? and o.nick =? and oi.waiting_list = true ";
        String st2 = "select * from Order_items oi, Orders o where oi.order_id = o.id and o.purchase_id = ? and oi.waiting_list = true ";
        String str = nick != null ? st1 : st2;

        try (Connection conn = getConnection(); PreparedStatement stat = conn.prepareStatement(str);) {
            stat.setLong(1, getLastPurchaseId());
            if (nick != null) {
                stat.setString(2, nick);
            }
            List<OrderItemModel> lst = new ArrayList<>();
            try (ResultSet rs = stat.executeQuery()) {
                while (rs != null && rs.next()) {
                    OrderItemModel mod = new OrderItemModel();
                    mod.setId(rs.getInt("id"));
                    mod.setFabricId(rs.getInt("fabric_id"));
                    mod.setOrderId(rs.getInt("order_id"));
                    mod.setCode(rs.getString("fabric_code"));
                    mod.setLength(rs.getDouble("length"));
                    mod.setWaitingList(rs.getBoolean("waiting_list"));
                    lst.add(mod);
                }
            }

            if (lst.isEmpty()) {
                return updatedRows;
            }

            String str1
                    = "select sum(roll_length) as total_length, count(roll_length) as roll_count, 0 as need_length, 0 as request_count  "
                    + " from Fabrics where purchase_id = ? and code = ? and  visible = true "
                    + " union"
                    + " select 0 as total_length, 0 as roll_count, sum(oi.length) as need_length, count(oi.length) as request_count "
                    + " from Order_items oi, Orders o  "
                    + " where o.id = oi.order_id and o.purchase_id=? and  oi.fabric_code = ?"
                    + " and o.order_date <= (select a.order_date from Orders a, Order_items s where s.order_id=a.id and s.id=?)";
            try (PreparedStatement stat1 = conn.prepareStatement(str1);) {
                for (OrderItemModel mod : lst) {
                    stat1.setLong(1, getLastPurchaseId());
                    stat1.setString(2, mod.getCode());
                    stat1.setLong(3, getLastPurchaseId());
                    stat1.setString(4, mod.getCode());
                    stat1.setLong(5, mod.getId());
                    double totalLength = 0;
                    int rollCount = 0;
                    double needLength = 0;
                    int requestCount = 0;
                    try (ResultSet rs = stat1.executeQuery()) {
                        if (rs != null && rs.next()) {
                            totalLength = rs.getDouble("total_length");
                            rollCount = rs.getInt("roll_count");
                        }
                        if (rs != null && rs.next()) {
                            needLength = rs.getDouble("need_length");
                            requestCount = rs.getInt("request_count");
                        }
                    }
                    if (totalLength < needLength) {
                        continue;
                    }

                    String str3
                            = "select id as fabric_id, roll_length, 0 as  bougth_length, roll_number from Fabrics "
                            + " where purchase_id = ? and code = ? and  visible = true and id not in ("
                            + " select oi.fabric_id  from Order_items oi"
                            + " where oi.fabric_id in ( select id from Fabrics where purchase_id = ? and code = ? and  visible = true order by roll_number)"
                            + " group by oi.fabric_id)"
                            + " union"
                            + " select oi.fabric_id, f.roll_length, sum(oi.length) as bougth_length, f.roll_number from Order_items oi, Fabrics f "
                            + " where f.id=oi.fabric_id and oi.fabric_id in ( "
                            + " select id from Fabrics where purchase_id = ? and code = ? and  visible = true order by roll_number)"
                            + " group by oi.fabric_id  "
                            + " order by roll_number asc";
                    List<FabricModel> fabrics = new ArrayList<>();
                    try (PreparedStatement stat3 = conn.prepareStatement(str3)) {
                        stat3.setLong(1, getLastPurchaseId());
                        stat3.setString(2, mod.getCode());
                        stat3.setLong(3, getLastPurchaseId());
                        stat3.setString(4, mod.getCode());
                        stat3.setLong(5, getLastPurchaseId());
                        stat3.setString(6, mod.getCode());
                        try (ResultSet rs = stat3.executeQuery()) {
                            while (rs != null && rs.next()) {
                                FabricModel m = new FabricModel();
                                m.setId(rs.getInt("fabric_id"));
//                                m.setCode(rs.getString("code"));
                                m.setRollNumber(rs.getInt("roll_number"));
                                m.setRollLength(rs.getDouble("roll_length"));
                                m.setBougthLength(rs.getDouble("bougth_length"));
                                //                               m.setVisible(rs.getBoolean("visible"));
//                                m.setApproved(rs.getBoolean("approved"));
                                fabrics.add(m);
                            }
                        }
                    }

                    int desiredId = -1;
                    for (FabricModel m : fabrics) { //ищем нужный
                        if (mod.getLength() <= (m.getRollLength() - m.getBougthLength())) {
                            desiredId = m.getId();
                            break;
                        }
                    }

                    String str4 = "update Order_items set fabric_id=?, waiting_list = false  where id = ?";
                    if (desiredId != -1) { // нашли!
                        // один рулон, выделим метраж
                        try (PreparedStatement stat4 = conn.prepareStatement(str4)) {
                            stat4.setLong(1, desiredId);
                            stat4.setLong(2, mod.getId());
                            updatedRows += stat4.executeUpdate();
                        }
                    }
                }
            }
        } catch (SQLException ex) {
            updatedRows = 0;
            LOG.error(str, ex);
        }
        LOG.debug("End distributionWaitingList(), updated rows " + updatedRows);
        return updatedRows;

    }

    public String getOrderItemByNick(String nick, boolean waitingList) {
        LOG.debug("Begin getOrderItemByNick(), nick: " + nick);
        String resultStr = "";
        String str = "select i.fabric_code, i.length from Order_items i, Orders o where o.id = i.order_id and o.purchase_id = ? and o.nick = ? and i.waiting_list = ?";
        try (Connection conn = getConnection(); PreparedStatement stat = conn.prepareStatement(str);) {
            stat.setLong(1, getLastPurchaseId());
            stat.setString(2, nick);
            stat.setBoolean(3, waitingList);
            try (ResultSet rs = stat.executeQuery()) {
                while (rs != null && rs.next()) {
                    resultStr += rs.getString("fabric_code") + "-" + rs.getString("length") + ";";
                }
            }
        } catch (SQLException ex) {
            resultStr = null;
            LOG.error(str, ex);
        }
        LOG.debug("End getOrderItemByNick(), resultStr: " + resultStr);
        return resultStr;
    }

    public String getOrderInfoByNick(String nick) {
        LOG.debug("Begin getOrderInfoByNick(), nick: " + nick);
        String resultStr = "";
        String str = "select * from Orders where purchase_id = ? and nick = ?";
        try (Connection conn = getConnection(); PreparedStatement stat = conn.prepareStatement(str);) {
            stat.setLong(1, getLastPurchaseId());
            stat.setString(2, nick);
            try (ResultSet rs = stat.executeQuery()) {
                while (rs != null && rs.next()) {
                    resultStr += rs.getString("order_info");
                }
            }
        } catch (SQLException ex) {
            resultStr = null;
            LOG.error(str, ex);
        }
        LOG.debug("End getOrderInfoByNick(), resultStr: " + resultStr);
        return resultStr;
    }

    public List<String> getAllCustomerNames() {
        LOG.debug("Begin getAllCustomerNames()");
        List<String> res = new ArrayList<>();
        String str = " select distinct nick from Orders  where purchase_id = ? order by nick asc";

        try (Connection conn = getConnection(); PreparedStatement stat = conn.prepareStatement(str);) {
            stat.setLong(1, getLastPurchaseId());
            try (ResultSet rs = stat.executeQuery()) {
                while (rs != null && rs.next()) {
                    res.add(rs.getString("nick"));
                }
            }
        } catch (SQLException ex) {
            res = null;
            LOG.error(str, ex);
        }
        LOG.debug("End getAllCustomerNames()");
        return res;
    }

    public List<OrderItemModel> getOrderItemsByNick(String nick) {
        LOG.debug("Begin getOrderItemsByNick(), nick " + nick);
        List<OrderItemModel> res = new ArrayList<>();
        String str = "select oi.* from Orders o, Order_items oi where o.id=oi.order_id and o.purchase_id = ? and o.nick = ? and waiting_list = false";
        try (Connection conn = getConnection(); PreparedStatement stat = conn.prepareStatement(str);) {
            stat.setLong(1, getLastPurchaseId());
            stat.setString(2, nick);
            try (ResultSet rs = stat.executeQuery()) {
                while (rs != null && rs.next()) {
                    OrderItemModel mod = new OrderItemModel();
                    mod.setId(rs.getInt("id"));
                    mod.setFabricId(rs.getInt("fabric_id"));
                    mod.setCode(rs.getString("fabric_code"));
                    mod.setLength(rs.getDouble("length"));
                    mod.setWaitingList(rs.getBoolean("waiting_list"));
                    res.add(mod);
                }
            }
        } catch (SQLException ex) {
            res = null;
            LOG.error(str, ex);
        }
        LOG.debug("End getOrderItemsByNick() ");
        return res;

    }

    public List<OrderExtModel> getOrderItemsBy(int orderId) {
        LOG.debug("Begin getOrderItemsByNick(), orderId: " + orderId);
        List<OrderExtModel> res = new ArrayList<>();
        String str = " select oi.* from Order_items oi where oi.order_id=? order by oi.waiting_list asc";

        try (Connection conn = getConnection(); PreparedStatement stat = conn.prepareStatement(str);) {
            stat.setLong(1, orderId);
            try (ResultSet rs = stat.executeQuery()) {
                while (rs != null && rs.next()) {
                    OrderExtModel mod = new OrderExtModel();
                    mod.setId(rs.getInt("id"));
                    mod.setFabricId(rs.getInt("fabric_id"));
                    mod.setCode(rs.getString("fabric_code"));
                    mod.setLength(rs.getDouble("length"));
                    mod.setWaitingList(rs.getBoolean("waiting_list"));
                    res.add(mod);
                }
            }
        } catch (SQLException ex) {
            res = null;
            LOG.error(str, ex);
        }
        LOG.debug("End getOrderItemsByNick() ");
        return res;
    }

    public List<HistoryModel> getOrdersBy(String nick) {
        LOG.debug("Begin getOrdersBy(), nick: " + nick);
        List<HistoryModel> res = new ArrayList<>();
        String str = "select * from Orders where purchase_id = ? and nick = ? order by order_date asc";
        try (Connection conn = getConnection(); PreparedStatement stat = conn.prepareStatement(str);) {
            stat.setLong(1, getLastPurchaseId());
            stat.setString(2, nick);
            try (ResultSet rs = stat.executeQuery()) {
                while (rs != null && rs.next()) {
                    HistoryModel mod = new HistoryModel();
                    mod.setDate(rs.getString("order_date"));
                    mod.setAuthor(rs.getString("nick"));
                    mod.setInfo(rs.getString("order_info"));
                    mod.setNote(rs.getString("note"));
                    res.add(mod);
                }
            }
        } catch (SQLException ex) {
            res = null;
            LOG.error(str, ex);
        }
        LOG.debug("End getOrdersBy()");
        return res;
    }

    public List<OrderExtModel> getOrderExtsBy(String nick) {
        LOG.debug("Begin getOrderExtsBy(), nick: " + nick);
        List<OrderExtModel> res = new ArrayList<>();
        String str = "select distinct o.*, oi.id as item_id, oi.length, oi.waiting_list, oi.fabric_code from Orders o "
                + " right join Order_items oi on o.id=oi.order_id"
                + " where o.purchase_id =? and  o.nick = ?"
                + " order by o.order_date desc, oi.waiting_list asc, oi.fabric_code  asc ";
        try (Connection conn = getConnection(); PreparedStatement stat = conn.prepareStatement(str);) {
            stat.setLong(1, getLastPurchaseId());
            stat.setString(2, nick);
            try (ResultSet rs = stat.executeQuery()) {
                while (rs != null && rs.next()) {
                    OrderExtModel mod = new OrderExtModel();
                    mod.setDate(rs.getString("order_date"));
                    mod.setInfo(rs.getString("order_info"));
                    mod.setNote(rs.getString("note"));
                    mod.setId(rs.getInt("id"));
                    mod.setItemId(rs.getInt("item_id"));
                    mod.setCode(rs.getString("fabric_code"));
                    mod.setLength(rs.getDouble("length"));
                    mod.setWaitingList(rs.getBoolean("waiting_list"));
                    res.add(mod);
                }
            }
        } catch (SQLException ex) {
            res = null;
            LOG.error(str, ex);
        }
        LOG.debug("End getOrderExtsBy()");
        return res;
    }

    public Set<String> getUniqueFabrics() {
        LOG.debug("Begin getUniqueFabrics()");
        Set<String> res = new HashSet<>();
        String str = "select distinct code from Fabrics where purchase_id = ? and visible = 1 order by code desc";
        try (Connection conn = getConnection(); PreparedStatement stat = conn.prepareStatement(str)) {
            stat.setLong(1, getLastPurchaseId());
            try (ResultSet rs = stat.executeQuery()) {
                while (rs != null && rs.next()) {
                    res.add(rs.getString("code"));
                }
            }
        } catch (SQLException ex) {
            res = null;
            LOG.error(str, ex);
        }
        LOG.debug("End getUniqueFabrics()");
        return res;
    }

    /*
    public Map<String, String> getPurchasesMap() {
        LOG.debug("Begin getPurchasesMap()");
        Map<String, String> res = new HashMap<String, String>();
        String str = "select * from Purchases order by id desc";
        try (Connection conn = getConnection(); Statement stat = conn.createStatement(); ResultSet rs = stat.executeQuery(str)) {
            while (rs != null && rs.next()) {
                res.put(rs.getString("id"), rs.getString("description"));
            }
        } catch (SQLException ex) {
            res = null;
            LOG.error(str, ex);
        }
        LOG.debug("End getPurchasesMap()");
        return res;
    }
     */
 /*
    Выдает список марафонов 
     */
    public List<SelectItem> getPurchasesList() {
        LOG.debug("Begin getPurchasesList()");
        List<SelectItem> res = new ArrayList<SelectItem>();
        String str = "select * from Purchases order by id desc";
        try (Connection conn = getConnection(); Statement stat = conn.createStatement(); ResultSet rs = stat.executeQuery(str)) {
            while (rs != null && rs.next()) {
                res.add(new SelectItem(rs.getString("id"), rs.getString("description")));
            }
        } catch (SQLException ex) {
            res = null;
            LOG.error(str, ex);
        }
        LOG.debug("End getPurchasesList()");
        return res;
    }

    /*
    Выдает список марафонов 
     */
    public List<PurchaseModel> getPurchases() {
        LOG.debug("Begin getPurchases()");
        List<PurchaseModel> res = new ArrayList<PurchaseModel>();
        String str = "select * from Purchases order by id desc";
        try (Connection conn = getConnection(); Statement stat = conn.createStatement(); ResultSet rs = stat.executeQuery(str)) {
            while (rs != null && rs.next()) {
                PurchaseModel mod = new PurchaseModel();
                mod.setId(rs.getInt("id"));
                mod.setDate(rs.getDate("date"));
                mod.setDescription(rs.getString("description"));
                res.add(mod);
            }
        } catch (SQLException ex) {
            res = null;
            LOG.error(str, ex);
        }
        LOG.debug("End getPurchasesList()");
        return res;
    }

    public PurchaseModel getCurrentPurchase() {
        LOG.debug("Begin getCurrentPurchase()");
        PurchaseModel res = new PurchaseModel();
        String str = "select p.* from Settings s, Purchases p where p.id=s.value and s.name = 'CURRENT_PURCHASE_NUMBER'";
        try (Connection conn = getConnection(); Statement stat = conn.createStatement(); ResultSet rs = stat.executeQuery(str)) {
            while (rs != null && rs.next()) {
                res.setId(rs.getInt("id"));
                res.setDate(rs.getDate("date"));
                res.setDescription(rs.getString("description"));
            }
        } catch (SQLException ex) {
            res = null;
            LOG.error(str, ex);
        }
        LOG.debug("End getCurrentPurchase()");
        return res;
    }

    /*
    Выдает марафон по id 
     */
    public PurchaseModel getPurchaseBy(int id) {
        LOG.debug("Begin getPurchaseBy(" + id + ")");
        PurchaseModel res = new PurchaseModel();
        String str = "select * from Purchases where id=?";
        try (Connection conn = getConnection(); PreparedStatement stat = conn.prepareStatement(str);) {
            stat.setLong(1, id);
            try (ResultSet rs = stat.executeQuery()) {
                while (rs != null && rs.next()) {
                    res.setId(rs.getInt("id"));
                    res.setDate(rs.getDate("date"));
                    res.setDescription(rs.getString("description"));
                }
            }
        } catch (SQLException ex) {
            res = null;
            LOG.error(str, ex);
        }
        LOG.debug("End getPurchasesList()");
        return res;
    }

    public boolean setCurrentPurchase(long purchaseId) {
        LOG.debug("Begin setCurrentPurchase()");
        boolean result = true;
        String str = "update Settings set value = ? where name  = 'CURRENT_PURCHASE_NUMBER' ";
        try (Connection conn = getConnection(); PreparedStatement stat = conn.prepareStatement(str)) {
            stat.setString(1, Long.toString(purchaseId));
            result &= stat.executeUpdate() > 0;
        } catch (SQLException ex) {
            result = false;
            LOG.error(str, ex);
        }
        if (result) {
            setLastPurchaseId(purchaseId);
        }
        LOG.debug("End setCurrentPurchase()");
        return result;
    }

    public boolean savePurchase(String name) {
        LOG.debug("Begin savePurchase()");
        boolean result = true;
        String str = "insert into Purchases (description, date) values ( ?, CURRENT_DATE() )";
        try (Connection conn = getConnection(); PreparedStatement stat = conn.prepareStatement(str)) {
            stat.setString(1, name);
            result &= stat.executeUpdate() > 0;
        } catch (SQLException ex) {
            result = false;
            LOG.error(str, ex);
        }
        LOG.debug("End savePurchase()");
        return result;
    }

    /*
   Сохранение изменениий модели
     */
    public boolean updatePurchase(PurchaseModel mod) {
        LOG.debug("Begin updatePurchase()");
        boolean result = true;
        String str = "update Purchases set description = ? where id = ?";
        try (Connection conn = getConnection(); PreparedStatement stat = conn.prepareStatement(str)) {
            stat.setString(1, mod.getDescription());
            //               stat.setDate(2, new java.sql.Date(mod.getDate().getTime()));
            stat.setInt(2, mod.getId());
            result &= stat.executeUpdate() > 0;
        } catch (SQLException ex) {
            result = false;
            LOG.error(str, ex);
        }
        LOG.debug("End updatePurchase()");
        return result;
    }

    // try-with-resources block
    public boolean saveFabrics(List<FabricModel> lst) {
        LOG.debug("Begin saveFabrics()");
        boolean result = true;
        String str = "insert into Fabrics (purchase_id, code, roll_number, roll_length, visible, approved, price) values ( ?, ?, ?, ?, ?, ?, ? )";
        try (Connection conn = getConnection(); PreparedStatement stat = conn.prepareStatement(str)) {
            for (FabricModel mod : lst) {
                stat.setLong(1, getLastPurchaseId());
                stat.setString(2, mod.getCode());
                stat.setLong(3, mod.getRollNumber());
                stat.setDouble(4, mod.getRollLength());
                stat.setBoolean(5, mod.isVisible());
                stat.setBoolean(6, mod.isApproved());
                stat.setDouble(7, mod.getPrice());
                result &= stat.executeUpdate() > 0;
            }
        } catch (SQLException ex) {
            result = false;
            LOG.error(str, ex);
        }
        LOG.debug("End saveFabrics()");
        return result;
    }

    public boolean updateFabricModel(FabricModel mod) {
        LOG.debug("Begin updateFabricModel()");
        boolean result = true;
        String str = "update Fabrics set code = ?, roll_number = ?, roll_length = ?, visible = ?"
                + ", approved = ?, price=?, image_folder=?, image_path=?, image_title=? where id  = ? ";
        try (Connection conn = getConnection(); PreparedStatement stat = conn.prepareStatement(str)) {
            stat.setString(1, mod.getCode());
            stat.setLong(2, mod.getRollNumber());
            stat.setDouble(3, mod.getRollLength());
            stat.setBoolean(4, mod.isVisible());
            stat.setBoolean(5, mod.isApproved());
            stat.setDouble(6, mod.getPrice());
            stat.setString(7, mod.getImageFolder());
            stat.setString(8, mod.getImagePath());
            stat.setString(9, mod.getImageTitle());
            stat.setInt(10, mod.getId());
            result &= stat.executeUpdate() > 0;
        } catch (SQLException ex) {
            result = false;
            LOG.error(str, ex);
        }

        LOG.debug("End updateFabricModel()");
        return result;
    }

    public boolean approvedFabrics() {
        LOG.debug("Begin approvedFabrics()");
        boolean result = true;
        String str = "update Fabrics set approved = true where id  = ? ";
        String str2 = "select q.id from ("
                + " select  f.id, f.roll_length,  sum(oi.length)  as sum"
                + " from Order_items oi, Fabrics f"
                + " where oi.fabric_id = f.id and  f.purchase_id = ? and f.visible=true and f.approved=false "
                + " group by f.id ) as q"
                + " where q.roll_length = q.sum";

        try (Connection conn = getConnection(); PreparedStatement stat = conn.prepareStatement(str); PreparedStatement stat2 = conn.prepareStatement(str2)) {
            stat2.setLong(1, getLastPurchaseId());
            try (ResultSet rs = stat2.executeQuery()) {
                while (rs != null && rs.next()) {
                    int id = (rs.getInt("id"));
                    stat.setInt(1, id);
                    result &= stat.executeUpdate() > 0;
                }
            }
        } catch (SQLException ex) {
            result = false;
            LOG.error(str, ex);
        }
        LOG.debug("End approvedFabrics()");
        return result;
    }

    public boolean updateFabrics(List<FabricModel> lst) {
        LOG.debug("Begin updateFabrics()");
        boolean result = true;
        String str = "update Fabrics set code = ?, roll_number = ?, roll_length = ?, visible = ?, approved = ?, price = ? where id  = ? ";
        try (Connection conn = getConnection(); PreparedStatement stat = conn.prepareStatement(str)) {
            for (FabricModel mod : lst) {
                stat.setString(1, mod.getCode());
                stat.setLong(2, mod.getRollNumber());
                stat.setDouble(3, mod.getRollLength());
                stat.setBoolean(4, mod.isVisible());
                stat.setBoolean(5, mod.isApproved());
                stat.setDouble(6, mod.getPrice());
                stat.setInt(7, mod.getId());
                result &= stat.executeUpdate() > 0;
            }
        } catch (SQLException ex) {
            result = false;
            LOG.error(str, ex);
        }
        LOG.debug("End updateFabrics()");
        return result;
    }

    public boolean visibleAllFabrics(List<FabricModel> lst) {
        LOG.debug("Begin visibleAllFabrics()");
        boolean result = true;
        String str = "update Fabrics set visible = true where id  = ? ";
        try (Connection conn = getConnection(); PreparedStatement stat = conn.prepareStatement(str)) {
            for (FabricModel mod : lst) {
                stat.setInt(1, mod.getId());
                result &= stat.executeUpdate() > 0;
            }
        } catch (SQLException ex) {
            result = false;
            LOG.error(str, ex);
        }
        LOG.debug("End visibleAllFabrics()");
        return result;
    }

    public List<HistoryModel> getHistoryList() {
        LOG.debug("Begin getHistoryList()");
        List<HistoryModel> res = new ArrayList<>();
        String str = "select * from Orders where purchase_id = ? order by order_date desc";
        try (Connection conn = getConnection(); PreparedStatement stat = conn.prepareStatement(str);) {
            stat.setLong(1, getLastPurchaseId());
            try (ResultSet rs = stat.executeQuery()) {
                while (rs != null && rs.next()) {
                    HistoryModel mod = new HistoryModel();
                    mod.setDate(rs.getString("order_date"));
                    mod.setAuthor(rs.getString("nick"));
                    mod.setInfo(rs.getString("order_info"));
                    mod.setNote(rs.getString("note"));
                    res.add(mod);
                }
            }
        } catch (SQLException ex) {
            res = null;
            LOG.error(str, ex);
        }
        LOG.debug("End getHistoryList()");
        return res;
    }

    public List<OrderItemModel> getCustomersByFabricId(int fabricId) {
        LOG.debug("Begin getCustomersByFabricId(), fabricId: " + fabricId);
        List<OrderItemModel> res = new ArrayList<>();
        String str = "(select oi.*, o.nick from Order_items oi, Orders o, Fabrics f"
                + " where oi.order_id=o.id and o.purchase_id=? and oi.fabric_id =f.id  and f.id=?"
                + " order by o.order_date asc)"
                + " union "
                + " (select oi.*, o.nick from Order_items oi, Orders o, Fabrics f"
                + " where oi.order_id=o.id and o.purchase_id=? and oi.fabric_code =f.code and f.id=? and waiting_list=true"
                + " order by o.order_date asc)";

        try (Connection conn = getConnection(); PreparedStatement stat = conn.prepareStatement(str);) {
            stat.setLong(1, getLastPurchaseId());
            stat.setLong(2, fabricId);
            stat.setLong(3, getLastPurchaseId());
            stat.setLong(4, fabricId);
            try (ResultSet rs = stat.executeQuery()) {
                while (rs != null && rs.next()) {
                    OrderItemModel mod = new OrderItemModel();
                    mod.setId(rs.getInt("id"));
                    mod.setFabricId(rs.getInt("fabric_id"));
                    mod.setCode(rs.getString("fabric_code"));
                    mod.setLength(rs.getDouble("length"));
                    mod.setWaitingList(rs.getBoolean("waiting_list"));
                    mod.setNick(rs.getString("nick"));
                    res.add(mod);
                }
            }
        } catch (SQLException ex) {
            res = null;
            LOG.error(str, ex);
        }
        LOG.debug("End getCustomersByFabricId()");
        return res;
    }

    public FabricModel getFabricModelById(int id) {
        LOG.debug("Begin getFabricModelById(), id: " + id);
        FabricModel mod = new FabricModel();
        String str = " select * from Fabrics where id=? ";
        try (Connection conn = getConnection(); PreparedStatement stat = conn.prepareStatement(str);) {
            stat.setLong(1, id);
            try (ResultSet rs = stat.executeQuery()) {
                while (rs != null && rs.next()) {
                    mod.setId(rs.getInt("id"));
                    mod.setCode(rs.getString("code"));
                    mod.setRollNumber(rs.getInt("roll_number"));
                    mod.setRollLength(rs.getDouble("roll_length"));
                    mod.setVisible(rs.getBoolean("visible"));
                    mod.setApproved(rs.getBoolean("approved"));
                    mod.setPrice(rs.getDouble("price"));
                }
            }

        } catch (SQLException ex) {
            mod = null;
            LOG.error(str, ex);
        }
        LOG.debug("End getFabricModelById()");
        return mod;
    }

    public List<FabricModel> getAllFabrics() {
        LOG.debug("Begin getAllFabrics()");
        List<FabricModel> res = new ArrayList<>();
        String str
                = "  select f.* , sum(oi.length) as bougth_length "
                + " from Fabrics f left join Order_items oi on f.id=oi.fabric_id "
                + " where  f.purchase_id = ?"
                + " group by f.id "
                + " order by f.code asc, f.roll_number asc";

        try (Connection conn = getConnection(); PreparedStatement stat = conn.prepareStatement(str);) {
            stat.setLong(1, getLastPurchaseId());
            try (ResultSet rs = stat.executeQuery()) {
                while (rs != null && rs.next()) {
                    FabricModel mod = new FabricModel();
                    mod.setId(rs.getInt("id"));
                    mod.setCode(rs.getString("code"));
                    mod.setRollNumber(rs.getInt("roll_number"));
                    mod.setRollLength(rs.getDouble("roll_length"));
                    mod.setVisible(rs.getBoolean("visible"));
                    mod.setApproved(rs.getBoolean("approved"));
                    mod.setBougthLength(rs.getDouble("bougth_length"));
                    mod.setPrice(rs.getDouble("price"));
                    mod.setImageFolder(rs.getString("image_folder"));
                    mod.setImagePath(rs.getString("image_path"));
                    mod.setImageTitle(rs.getString("image_title"));
//                mod.setNeedLength(rs.getDouble("need_length"));
                    res.add(mod);
                }
            }

        } catch (SQLException ex) {
            res = null;
            LOG.error(str, ex);
        }
        LOG.debug("End getAllFabrics()");
        return res;
    }

    public List<FabricModel> getActiveFabrics() {
        LOG.debug("Begin getActiveFabrics()");
        List<FabricModel> res = new ArrayList<>();
        String str = "  select t1.*,  t2.need_length	from"
                + " ( select f.* , sum(oi.length) as bougth_length "
                + " from Fabrics f left join Order_items oi on f.id=oi.fabric_id "
                + " where  f.purchase_id = ? and f.visible = true"
                + " group by f.id "
                + " ) as t1"
                + " left join "
                + " ("
                + " select oi.fabric_code , sum(oi.length) as need_length "
                + " from Orders o , Order_items oi"
                + " where  o.purchase_id = ? and  o.id=oi.order_id  and oi.waiting_list=true"
                + " group by oi.fabric_code"
                + " ) as t2"
                + " on t1.code = t2.fabric_code"
                + " order by t1.approved asc, t1.code asc, t1.roll_number asc";

        try (Connection conn = getConnection(); PreparedStatement stat = conn.prepareStatement(str);) {
            stat.setLong(1, getLastPurchaseId());
            stat.setLong(2, getLastPurchaseId());
            try (ResultSet rs = stat.executeQuery()) {
                while (rs != null && rs.next()) {
                    FabricModel mod = new FabricModel();
                    mod.setId(rs.getInt("id"));
                    mod.setCode(rs.getString("code"));
                    mod.setRollNumber(rs.getInt("roll_number"));
                    mod.setRollLength(rs.getDouble("roll_length"));
                    mod.setVisible(rs.getBoolean("visible"));
                    mod.setApproved(rs.getBoolean("approved"));
                    mod.setBougthLength(rs.getDouble("bougth_length"));
                    mod.setNeedLength(rs.getDouble("need_length"));
                    mod.setPrice(rs.getDouble("price"));
                    res.add(mod);
                }
            }

        } catch (SQLException ex) {
            res = null;
            LOG.error(str, ex);
        }
        LOG.debug("End getActiveFabrics()");
        return res;
    }

    public List<FabricAvailModel> getAvailFabricListForCustomers() {
        LOG.debug("Begin getAvailFabricListForCustomers()");
        List<FabricAvailModel> res = new ArrayList<>();
        String str = " select k.* , k2.total_length, "
                + " case when total_length is null then sum_roll_length when total_length is not  null then (sum_roll_length - total_length) end "
                + " as remainder , ff.image_path, ff.image_folder, ff.image_title , k2.total_length"
                + "  from ( select f.code , sum(roll_length) as sum_roll_length"
                + " from Fabrics f "
                + " where  f.purchase_id = ? and visible=true"
                + " group by f.code ) as k "
                + " left join "
                + " ( select  oi.fabric_code, sum(oi.length) as total_length"
                + " from Order_items oi, Orders o "
                + " where o.purchase_id = ? and o.id=oi.order_id"
                + " group by oi.fabric_code  ) as k2 "
                + " on k.code = k2.fabric_code "
                + " left join Fabrics ff on k.code = ff.code and ff.purchase_id = ? "
                + " where total_length is null or  sum_roll_length > total_length order by k.code asc;";

        try (Connection conn = getConnection(); PreparedStatement stat = conn.prepareStatement(str);) {

            stat.setLong(1, getLastPurchaseId());
            stat.setLong(2, getLastPurchaseId());
            stat.setLong(3, getLastPurchaseId());
            try (ResultSet rs = stat.executeQuery()) {
                while (rs != null && rs.next()) {
                    FabricAvailModel mod = new FabricAvailModel();
                    mod.setCode(rs.getString("code"));
                    mod.setImageFolder(rs.getString("image_folder"));
                    mod.setImagePath(rs.getString("image_path"));
                    mod.setImageTitle(rs.getString("image_title"));
                    mod.setRollLength(rs.getDouble("sum_roll_length"));
                    mod.setTotalLength(rs.getDouble("total_length"));
                    mod.setRemainder(rs.getDouble("remainder"));
                    res.add(mod);
                }
            }

        } catch (SQLException ex) {
            res = null;
            LOG.error(str, ex);
        }

        LOG.debug("End getAvailFabricListForCustomers()");
        return res;
    }

    public List<Report1Model> getReport1() {
        LOG.debug("Begin getReport1()");
        List<Report1Model> res = new ArrayList<>();
        String str = " select t1.*, t3.info  from"
                + " ( select f.* , sum(oi.length) as bougth_length "
                + " from Fabrics f left join Order_items oi on f.id=oi.fabric_id "
                + " where  f.purchase_id = ? and f.approved=true"
                + " group by f.id ) as t1"
                + " left join "
                + "  ( SELECT t2.fabric_id, GROUP_CONCAT(t2.info) as info  from"
                + " ( select oii.*, o.nick, CONCAT(o.nick, ' ', length, ' ') as info"
                + " from Order_items oii, Orders o"
                + " where o.id=oii.order_id and o.purchase_id = ?"
                + " order by o.order_date desc) as t2"
                + " group by t2.fabric_id  ) as t3"
                + "  on t1.id = t3.fabric_id"
                + " order by t1.code asc, t1.roll_number asc";

        try (Connection conn = getConnection(); PreparedStatement stat = conn.prepareStatement(str);) {
            stat.setLong(1, getLastPurchaseId());
            stat.setLong(2, getLastPurchaseId());
            try (ResultSet rs = stat.executeQuery()) {
                while (rs != null && rs.next()) {
                    Report1Model mod = new Report1Model();
                    mod.setId(rs.getInt("id"));
                    mod.setCode(rs.getString("code"));
                    mod.setRollNumber(rs.getInt("roll_number"));
                    mod.setRollLength(rs.getDouble("roll_length"));
                    mod.setBougthLength(rs.getDouble("bougth_length"));
                    mod.setInfo(rs.getString("info"));
                    res.add(mod);
                }
            }

        } catch (SQLException ex) {
            res = null;
            LOG.error(str, ex);
        }
        LOG.debug("End getReport1()");
        return res;
    }

    public List<Report1Model> getReportGeneralView(double[] mas) {

        LOG.debug("Begin getReportGeneralView()");
        List<Report1Model> res = new ArrayList<>();
        String str = "  select t1.code, t1.roll_number, t1.roll_length, t1.bougth_length, t3.info,"
                + " case when t1.approved = true then 3 when t1.approved = false then 1 end as level"
                + " from"
                + " ( select f.* , sum(oi.length) as bougth_length "
                + " from Fabrics f left join Order_items oi on f.id=oi.fabric_id "
                + " where  f.purchase_id = ? and f.visible=true"
                + " group by f.id ) as t1"
                + " left join "
                + "  ( SELECT t2.fabric_id, GROUP_CONCAT(t2.info) as info  from"
                + " ( select oii.*, o.nick, CONCAT(o.nick, ' ', length) as info"
                + " from Order_items oii, Orders o"
                + " where o.id=oii.order_id  and o.purchase_id= ?"
                + " order by o.order_date asc ) as t2"
                + " group by t2.fabric_id  ) as t3"
                + "  on t1.id = t3.fabric_id"
                + " union"
                + " SELECT s.fabric_code as code, 0 as roll_number, 0 as roll_length, sum(s.length) as bougth_length,"
                + " GROUP_CONCAT(s.info) as info, 2 as level     from"
                + " (  select oi1.*, o1.nick, CONCAT(o1.nick, ' ', length) as info"
                + " from Order_items oi1, Orders o1"
                + " where o1.id=oi1.order_id and o1.purchase_id= ? and oi1.waiting_list=true"
                + " order by o1.order_date asc ) as s"
                + " group by code "
                + " order by  level asc,   code asc, roll_number asc";

        String str2 = " select  sum(oi.length) as bougth_length , 3 as level  "
                + " from Fabrics f left join Order_items oi on f.id=oi.fabric_id "
                + " where  f.purchase_id = ? and f.visible=true and f.approved = true"
                + "  union"
                + "  select  sum(oi.length) as bougth_length , 1 as level  "
                + " from Fabrics f left join Order_items oi on f.id=oi.fabric_id "
                + " where  f.purchase_id = ? and f.visible=true and f.approved = false"
                + "  union"
                + "  SELECT  sum(oi1.length) as bougth_length, 2 as level    "
                + " from Order_items oi1, Orders o1"
                + " where o1.id=oi1.order_id and o1.purchase_id= ? and oi1.waiting_list=true";

        try (Connection conn = getConnection(); PreparedStatement stat = conn.prepareStatement(str); PreparedStatement stat2 = conn.prepareStatement(str2);) {
            stat.setLong(1, getLastPurchaseId());
            stat.setLong(2, getLastPurchaseId());
            stat.setLong(3, getLastPurchaseId());
            try (ResultSet rs = stat.executeQuery()) {
                while (rs != null && rs.next()) {
                    Report1Model mod = new Report1Model();
                    mod.setLevel(rs.getInt("level"));
                    mod.setCode(rs.getString("code"));
                    mod.setRollNumber(rs.getInt("roll_number"));
                    mod.setRollLength(rs.getDouble("roll_length"));
                    mod.setBougthLength(rs.getDouble("bougth_length"));
                    mod.setInfo(rs.getString("info"));
                    res.add(mod);
                }
            }

            stat2.setLong(1, getLastPurchaseId());
            stat2.setLong(2, getLastPurchaseId());
            stat2.setLong(3, getLastPurchaseId());
            try (ResultSet rs = stat2.executeQuery()) {
                while (rs != null && rs.next()) {
                    mas[rs.getInt("level") - 1] = rs.getDouble("bougth_length");
                }
            }

        } catch (SQLException ex) {
            res = null;
            LOG.error(str, ex);
        }
        LOG.debug("End getReportGeneralView()");
        return res;
    }

    public boolean deleteItemBy(int itemId) {
        LOG.debug("Begin deleteItemBy(), itemId: " + itemId);
        boolean result = true;
        String str = "delete from Order_items where id = ? ";
        try (Connection conn = getConnection(); PreparedStatement stat = conn.prepareStatement(str)) {
            stat.setLong(1, itemId);
            result &= stat.executeUpdate() > 0;
        } catch (SQLException ex) {
            result = false;
            LOG.error(str, ex);
        }
        LOG.debug("End deleteItemBy(), result: " + result);
        return result;
    }

    public boolean deleteOrderBy(int orderId) {
        LOG.debug("Begin deleteOrderBy(), orderId: " + orderId);
        boolean result = true;
        String str = "delete from Order_items where order_id = ? ";
        try (Connection conn = getConnection(); PreparedStatement stat = conn.prepareStatement(str)) {
            stat.setLong(1, orderId);
            result &= stat.executeUpdate() > 0;
        } catch (SQLException ex) {
            result = false;
            LOG.error(str, ex);
        }
        LOG.debug("End deleteOrderBy(), result: " + result);
        return result;
    }

    public boolean updateOrderItemModel(OrderItemModel mod) {
        LOG.debug("Begin updateOrderItemModel()");
        boolean result = true;
        String str = "update Order_items set length = ?, waiting_list = ?, fabric_id = ? where id  = ? ";
        try (Connection conn = getConnection(); PreparedStatement stat = conn.prepareStatement(str)) {
            stat.setDouble(1, mod.getLength());
            stat.setBoolean(2, mod.isWaitingList());
            if (mod.getFabricId() == -1) {
                stat.setObject(3, null);
            } else {
                stat.setInt(3, mod.getFabricId());
            }
            stat.setInt(4, mod.getId());
            result &= stat.executeUpdate() > 0;
        } catch (SQLException ex) {
            result = false;
            LOG.error(str, ex);
        }
        OrderItemModel savedModel = getOrderItemById(mod.getId());
        LOG.debug("End updateOrderItemModel(): old model:" + mod.toString());
        LOG.debug("End updateOrderItemModel(): saved model:" + savedModel.toString());
        return result;
    }

    public boolean giveOrderItem(String customerFrom, String customerTo, OrderItemModel mod, double length) {
        LOG.debug("Begin giveOrderItem(), customerFrom->v" + customerFrom + "->" + customerTo);
        boolean result = true;
        /*        String str = "update Order_items set length = ?, waiting_list = ? where id  = ? ";
        try (Connection conn = getConnection(); PreparedStatement stat = conn.prepareStatement(str)) {
            stat.setDouble(1, mod.getLength());
            stat.setBoolean(2, mod.isWaitingList());
            stat.setInt(3, mod.getId());
            result &= stat.executeUpdate() > 0;
        } catch (SQLException ex) {
            result = false;
            LOG.error(str, ex);
        }
         */
        LOG.debug("End giveOrderItem()");
        return result;
    }

    public List<Report2Model> getReport2() {
        LOG.debug("Begin getReport2()");
        List<Report2Model> res = new ArrayList<>();
        String str = " SELECT t1.nick, sum(t1.length) as length, sum(t1.otrez_count) as otrez_count, "
                + " sum(t1.otrez_amount) as otrez_amount, GROUP_CONCAT(t1.info) as info "
                + " from (select oi.*, o.nick, 1 as otrez_count, (oi.length*f.price) as otrez_amount, CONCAT(oi.fabric_code, ' - ', length, ' ; ') as info "
                + "  from Order_items oi, Orders o, Fabrics f "
                + " where o.id=oi.order_id and o.purchase_id = ? and f.id=oi.fabric_id and f.approved=true "
                + "  order by o.order_date desc ) as t1 "
                + "  group by nick order by t1.nick asc ";
        try (Connection conn = getConnection(); PreparedStatement stat = conn.prepareStatement(str);) {
            stat.setLong(1, getLastPurchaseId());
            try (ResultSet rs = stat.executeQuery()) {
                while (rs != null && rs.next()) {
                    Report2Model mod = new Report2Model();
                    //            mod.setId(rs.getInt("id"));
                    mod.setNick(rs.getString("nick"));
                    mod.setWeavingCutCount(rs.getInt("otrez_count"));
                    mod.setAmount(rs.getDouble("otrez_amount"));
                    mod.setWeavingCutLength(rs.getDouble("length"));
                    mod.setInfo(rs.getString("info"));
                    res.add(mod);
                }
            }

        } catch (SQLException ex) {
            res = null;
            LOG.error(str, ex);
        }

        LOG.debug("End getReport2()");
        return res;
    }

    public OrderItemModel getOrderItemById(int orderItemId) {
        LOG.debug("Begin getOrderItemById(), orderItemId= " + orderItemId);
        OrderItemModel res = null;
        String str = "select oi.*, o.nick from  Order_items oi, Orders o where o.id=oi.order_id and oi.id = ? ";
        try (Connection conn = getConnection(); PreparedStatement stat = conn.prepareStatement(str);) {
            stat.setInt(1, orderItemId);
            try (ResultSet rs = stat.executeQuery()) {
                while (rs != null && rs.next()) {
                    OrderItemModel mod = new OrderItemModel();
                    mod.setId(rs.getInt("id"));
                    mod.setFabricId(rs.getInt("fabric_id"));
                    mod.setOrderId(rs.getInt("order_id"));
                    mod.setCode(rs.getString("fabric_code"));
                    mod.setLength(rs.getDouble("length"));
                    mod.setWaitingList(rs.getBoolean("waiting_list"));
                    mod.setNick(rs.getString("nick"));
                    res = mod;
                }
            }
        } catch (SQLException ex) {
            res = null;
            LOG.error(str, ex);
        }
        LOG.debug("End getOrderItemsByNick() " + res.toString());
        return res;

    }

    /*
     Устанавливает цену price для fabrics
     */
    public boolean assignPrice(List<FabricModel> lst, double price) {
        LOG.debug("Begin assignFabrics()");
        boolean result = true;
        String str = "update Fabrics set price = ? where id  = ? ";
        try (Connection conn = getConnection(); PreparedStatement stat = conn.prepareStatement(str)) {
            for (FabricModel mod : lst) {
                stat.setDouble(1, price);
                stat.setInt(2, mod.getId());
                result &= stat.executeUpdate() > 0;
            }
        } catch (SQLException ex) {
            result = false;
            LOG.error(str, ex);
        }
        LOG.debug("End assignFabrics()");
        return result;
    }

}
