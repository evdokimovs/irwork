package ru.irwork.lets;

import ru.irwork.lets.model.*;
import java.io.Serializable;
import java.util.*;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

/**
 *
 * @author irwork
 */
@ManagedBean
@SessionScoped
public class GiftBean implements Serializable {

    private static final long serialVersionUID = 1L;

    private String customerFrom = "";
    private String customerTo = "";
    private OrderItemModel selectedItem;
    private double length;
    private List<String> customers;
    private List<OrderItemModel> orderItems;

    @ManagedProperty(value = "#{letsBuyApp}")
    private LetsBuyApp app;
    //This method is required for dependency injection (ManagedProperty)

    public void setApp(LetsBuyApp appBean) {
        this.app = appBean;
    }

    public void giftItem() {
        //resultMessage = new FacesMessage("Сохранение :"   "Данные не сохранены.")
        FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "\u0421\u043e\u0445\u0440\u0430\u043d\u0435\u043d\u0438\u0435 :", "\u0414\u0430\u043d\u043d\u044b\u0435 \u043d\u0435 \u0441\u043e\u0445\u0440\u0430\u043d\u0435\u043d\u044b.");

        boolean updated = app.giveOrderItem(customerFrom, customerTo, selectedItem, length);

        if (updated) {
            //resultMessage = new FacesMessage("Сохранение :"   "Данные сохранены успешно.")
            msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "\u0421\u043e\u0445\u0440\u0430\u043d\u0435\u043d\u0438\u0435 :", "\u0423\u0441\u043f\u0435\u0448\u043d\u043e\u0435 \u0437\u0430\u0432\u0435\u0440\u0448\u0435\u043d\u0438\u0435 \u043e\u043f\u0435\u0440\u0430\u0446\u0438\u0438.");
        }
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    public void showItems() {
        orderItems = null;
        if (customerFrom == null || customerFrom.isEmpty()) {
            return;
        }
        orderItems = app.getOrderItemsByNick(customerFrom);
    }

    public String getCustomerFrom() {
        return customerFrom;
    }

    public void setCustomerFrom(String customerFrom) {
        this.customerFrom = customerFrom;
    }

    public String getCustomerTo() {
        return customerTo;
    }

    public void setCustomerTo(String customerTo) {
        this.customerTo = customerTo;
    }

    public OrderItemModel getSelectedItem() {
        return selectedItem;
    }

    public void setSelectedItem(OrderItemModel selectedItem) {
        this.selectedItem = selectedItem;
    }

    public double getLength() {
        return length;
    }

    public void setLength(double length) {
        this.length = length;
    }

    public List<String> getCustomers() {
        if (customers == null || customers.isEmpty()) {
            customers = app.getAllCustomerNames();
        }
        return customers;
    }

    public void setCustomers(List<String> customers) {
        this.customers = customers;
    }

    public List<OrderItemModel> getOrderItems() {
        return orderItems;
    }

    public void setOrderItems(List<OrderItemModel> orderItems) {
        this.orderItems = orderItems;
    }

}
