package ru.irwork.lets;

import java.io.Serializable;
import java.util.*;
import java.util.regex.PatternSyntaxException;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import org.apache.log4j.Logger;
import ru.irwork.lets.model.FabricAvailModel;
import ru.irwork.lets.model.HistoryModel;
import ru.irwork.lets.model.OrderExtModel;

/**
 *
 * @author irwork
 */
@ManagedBean
@SessionScoped
public class OrderBean implements Serializable {

    private static final Logger LOG = Logger.getLogger(OrderBean.class);
    private static final long serialVersionUID = 1L;
    private String page; //  Default include.
    private String user;
    private String order;
    private String orderedItem = "";
    private String expectedItem = "";
    private List<HistoryModel> histories;
    private List<HistoryModel> filteredHistories;
    private List<FabricAvailModel> fabricsAvail;

    @ManagedProperty(value = "#{letsBuyApp}")
    private LetsBuyApp app;
    //This method is required for dependency injection (ManagedProperty)

    public void setApp(LetsBuyApp appBean) {
        this.app = appBean;
    }

    @PostConstruct
    public void init() {
        this.page = "order_input";
    }

    public void showUserInfo() {
        LOG.debug( "begin showUserInfo()");
        if (getUser() == null || getUser().length() == 0) {
            return;
        }
        orderedItem = app.getOrderItemByNick(user, false);
        expectedItem = app.getOrderItemByNick(user, true);
        LOG.debug( "end showUserInfo()");

    }

    public void addOrder() {
        LOG.debug( "begin addOrder()");
        FacesMessage resultMessage = null;
        //проверка  выбран ли юзер
        if (getUser() == null || getUser().length() == 0) {
            //resultMessage = new FacesMessage("Ник некорректен. Укажите пользователя.");
            resultMessage = new FacesMessage("\u041d\u0438\u043a \u043d\u0435\u043a\u043e\u0440\u0440\u0435\u043a\u0442\u0435\u043d. \u0423\u043a\u0430\u0436\u0438\u0442\u0435 \u043f\u043e\u043b\u044c\u0437\u043e\u0432\u0430\u0442\u0435\u043b\u044f.");
            FacesContext.getCurrentInstance().addMessage(null, resultMessage);
            return;
        }

        // пользователь без заказа имеет права быть
        if (order == null || order.length() == 0) {
            orderedItem = app.getOrderItemByNick(user, false);
            expectedItem = app.getOrderItemByNick(user, true);
            return;
        }
        String note = "";
        boolean smallFlapExists = false;

        String str = order.replaceAll("\\s+", ""); // регулярные выражения \s - это "any whitespace symbol" , а + - это "один или более символ"
        str = str.replaceAll(",", "."); // регулярные выражения \s - это "any whitespace symbol" , а + - это "один или более символ"
        str=str.toUpperCase();

        // парсим заказ
        List<OrderExtModel> rez = new ArrayList<>();
        String items = "";
        try {
            String[] subStr;
            String delimeter = ";"; // Разделитель
            subStr = str.split(delimeter);
            String del = "-"; // Разделитель
            String[] sub;
            for (String obj : subStr) {
                items = obj;
                if (obj.length() == 0) {
                    continue;
                }
                sub = obj.split(del);
                if (sub.length != 2) {
                    throw new NumberFormatException();
                }
                OrderExtModel mod = new OrderExtModel();
                mod.setCode(sub[0]);
                double d = Double.parseDouble(sub[1]);
                if (d < 0.5) {
                    d = 0.5;
                    //resultMessage = new FacesMessage("Длина увеличена до 0.5 : ");
                    resultMessage = new FacesMessage("\u0414\u043b\u0438\u043d\u0430 \u0443\u0432\u0435\u043b\u0438\u0447\u0435\u043d\u0430 \u0434\u043e 0.5 : " + sub[0]);
                    FacesContext.getCurrentInstance().addMessage(null, resultMessage);
                    if (!smallFlapExists) {
                        note = "\u0414\u043b\u0438\u043d\u0430 \u0443\u0432\u0435\u043b\u0438\u0447\u0435\u043d\u0430 \u0434\u043e 0.5 : ";
                    }
                    note += sub[0] + ", ";
                    smallFlapExists = true;
                }
                mod.setLength(d);
                rez.add(mod);
            }

        } catch (PatternSyntaxException e) {
            //resultMessage = new FacesMessage("Некорректный формат ввода.");
            resultMessage = new FacesMessage("\u041d\u0435\u043a\u043e\u0440\u0440\u0435\u043a\u0442\u043d\u044b\u0439 \u0444\u043e\u0440\u043c\u0430\u0442 \u0432\u0432\u043e\u0434\u0430.");
            FacesContext.getCurrentInstance().addMessage(null, resultMessage);
            LOG.info("PatternSyntaxException ");
            return;
        } catch (NumberFormatException e) {
            //resultMessage = new FacesMessage("Ошибка в написании : "+fabric);
            resultMessage = new FacesMessage("\u041e\u0448\u0438\u0431\u043a\u0430 \u0432 \u043d\u0430\u043f\u0438\u0441\u0430\u043d\u0438\u0438 : " + items);
            FacesContext.getCurrentInstance().addMessage(null, resultMessage);
            LOG.info("NumberFormatException ");
            return;
        }

        if (rez.isEmpty()) {
            //resultMessage = new FacesMessage("Некорректный формат ввода.");
            resultMessage = new FacesMessage("\u041d\u0435\u043a\u043e\u0440\u0440\u0435\u043a\u0442\u043d\u044b\u0439 \u0444\u043e\u0440\u043c\u0430\u0442 \u0432\u0432\u043e\u0434\u0430.");
            FacesContext.getCurrentInstance().addMessage(null, resultMessage);
            LOG.info("isEmpty ");
            return;
        }

        boolean incorrectValueExists = false;
        Set<String> set = app.getUniqueFabrics();
        String incorrectValues = "";
        for (int i = rez.size() - 1; i > -1; i--) {
            OrderExtModel mod = rez.get(i);
            if (!set.contains(mod.getCode())) {
                if (!incorrectValueExists) {
                    // "Incorrect value: "
                    note += "\u041d\u0435 \u043d\u0430\u0439\u0434\u0435\u043d\u044b \u043d\u0430\u0438\u043c\u0435\u043d\u043e\u0432\u0430\u043d\u0438\u044f:";
                }
                incorrectValues += mod.getCode() + "; ";
                note += mod.getCode() + "; ";
                rez.remove(i);
                incorrectValueExists = true;
            }
        }

        if (incorrectValues.length() > 0) {
            //resultMessage = new FacesMessage("Не найдены наименования : ");
            resultMessage = new FacesMessage("\u041d\u0435 \u043d\u0430\u0439\u0434\u0435\u043d\u044b \u043d\u0430\u0438\u043c\u0435\u043d\u043e\u0432\u0430\u043d\u0438\u044f : " + incorrectValues);
            FacesContext.getCurrentInstance().addMessage(null, resultMessage);
            LOG.info("incorrectValues ");
        }
        if (rez.isEmpty()) {
            return;
        }

        {
            int insertedRowsNumber = app.saveOrder(user, order, rez, note);

            if (insertedRowsNumber > 0) {
                //resultMessage = new FacesMessage("Заказ добавлен.");
                resultMessage = new FacesMessage(" \u0417\u0430\u043a\u0430\u0437 \u0434\u043e\u0431\u0430\u0432\u043b\u0435\u043d.");
                setOrder(null);
                orderedItem = app.getOrderItemByNick(user, false);
                expectedItem = app.getOrderItemByNick(user, true);
            } else {
                //resultMessage = new FacesMessage("Не удалось оформить заказ.");
                resultMessage = new FacesMessage("\u041d\u0435 \u0443\u0434\u0430\u043b\u043e\u0441\u044c \u043e\u0444\u043e\u0440\u043c\u0438\u0442\u044c \u0437\u0430\u043a\u0430\u0437.");
            }
        }
        FacesContext.getCurrentInstance().addMessage(null, resultMessage);
        LOG.debug( "end addOrder()");
    }

    public String getPage() {
        return page;
    }

    public void setPage(String page) {
        this.page = page;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user.trim();
    }

    public String getOrder() {
        return order;
    }

    public void setOrder(String order) {
        this.order = order;
    }

    public String getOrderedItem() {
        this.orderedItem = app.getOrderItemByNick(user, false);
        if (orderedItem == null || orderedItem.length() == 0) {
//            expectedItem=" ...(не найдено)";
            orderedItem = " ...(\u043d\u0435 \u043d\u0430\u0439\u0434\u0435\u043d\u043e)";
        }
        return orderedItem;
    }

    public List<HistoryModel> getHistories() {
//        if(histories==null || histories.size()<=0)
        this.histories = app.getHistoryList();
        return histories;
    }

    public void setHistories(List<HistoryModel> histories) {
        this.histories = histories;
    }

    public List<HistoryModel> getFilteredHistories() {
        return filteredHistories;
    }

    public void setFilteredHistories(List<HistoryModel> filteredHistories) {
        this.filteredHistories = filteredHistories;
    }

    public void setOrderedItem(String orderedItem) {
        this.orderedItem = orderedItem;
    }

    public String getExpectedItem() {
        expectedItem = app.getOrderItemByNick(user, true);
        if (expectedItem == null || expectedItem.length() == 0) {
//            expectedItem=" ...(не найдено)";
            expectedItem = " ...(\u043d\u0435 \u043d\u0430\u0439\u0434\u0435\u043d\u043e)";
        }
        return expectedItem;
    }

    public void setExpectedItem(String expectedItem) {
        this.expectedItem = expectedItem;
    }

    public List<FabricAvailModel> getFabricsAvail() {
        this.fabricsAvail = app.getAvailFabricListForCustomers();
        return fabricsAvail;
    }

    public void setFabricsAvail(List<FabricAvailModel> fabricsAvail) {
        this.fabricsAvail = fabricsAvail;
    }

}
