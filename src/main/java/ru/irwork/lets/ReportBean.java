package ru.irwork.lets;

import java.io.Serializable;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import ru.irwork.lets.model.*;

/**
 *
 * @author Ira
 */
@ManagedBean
@SessionScoped
public class ReportBean implements Serializable {

    private static final long serialVersionUID = 1L;
    private List<Report1Model> reports1;
    private List<Report1Model> generalView;
    final double[] totalAmounts = new double[3];

    private List<Report2Model> reports2;
    
    
    @ManagedProperty(value = "#{letsBuyApp}")
    private LetsBuyApp app;
    //This method is required for dependency injection (ManagedProperty)

    public void setApp(LetsBuyApp appBean) {
        this.app = appBean;
    }

    public void refreshReports1() {
        this.reports1 = app.getReport1();
        //resultMessage = new FacesMessage("Обновлено :"   ")
        FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "\u041e\u0431\u043d\u043e\u0432\u043b\u0435\u043d\u043e :", "");
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    public void refreshGeneralView() {
        //resultMessage = new FacesMessage("Действие обновить:", "Выполнено.")
        FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "\u0414\u0435\u0439\u0441\u0442\u0432\u0438\u0435 \u043e\u0431\u043d\u043e\u0432\u0438\u0442\u044c :", "\u0412\u044b\u043f\u043e\u043b\u043d\u0435\u043d\u043e.");
        this.generalView = app.getReportGeneralView(totalAmounts);
        if (generalView == null) {
            //resultMessage = new FacesMessage("Действие обновить:", "Не выполнено.")
            msg = new FacesMessage(FacesMessage.SEVERITY_WARN, "\u0414\u0435\u0439\u0441\u0442\u0432\u0438\u0435 \u043e\u0431\u043d\u043e\u0432\u0438\u0442\u044c :",
                    "\u041d\u0435 \u0432\u044b\u043f\u043e\u043b\u043d\u0435\u043d\u043e.");
        }
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    public List<Report1Model> getReports1() {
        if (reports1 == null || reports1.isEmpty()) {
            this.reports1 = app.getReport1();
        }
        return reports1;
    }

    public void setReports1(List<Report1Model> reports1) {
        this.reports1 = reports1;
    }

    public List<Report1Model> getGeneralView() {
        if (generalView == null || generalView.isEmpty()) {
            this.generalView = app.getReportGeneralView(totalAmounts);
        }
        return generalView;
    }

    public void setGeneralView(List<Report1Model> generalView) {
        this.generalView = generalView;
    }

    public double totalAmount(int level) {
        return totalAmounts[level - 1];
    }

    public void refreshReports2() {
        this.reports2 = app.getReport2();
        //resultMessage = new FacesMessage("Обновлено :"   ")
        FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "\u041e\u0431\u043d\u043e\u0432\u043b\u0435\u043d\u043e :", "");
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    public List<Report2Model> getReports2() {
        if (reports2 == null || reports2.isEmpty()) {
            this.reports2 = app.getReport2();
        }
        return reports2;
    }

    public void setReports2(List<Report2Model> reports2) {
        this.reports2 = reports2;
    }

}
