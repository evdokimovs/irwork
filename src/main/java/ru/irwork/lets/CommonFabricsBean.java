package ru.irwork.lets;

import ru.irwork.lets.model.*;
import java.io.Serializable;
import java.util.*;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import org.apache.log4j.Logger;


/**
 *
 * @author irwork
 */
@ManagedBean
@SessionScoped
public class CommonFabricsBean implements Serializable {

    private static final Logger LOG = Logger.getLogger(CommonFabricsBean.class);
    private static final long serialVersionUID = 1L;

    private int selectedFabricId;
    private List<FabricModel> fabrics;
    private List<OrderItemModel> customers;
    private OrderItemModel selectedFabricItem;
    int count = 0;
    private HashMap<Integer,List<OrderItemModel>> customersForFabrics = new HashMap<>(50);
   

    @ManagedProperty(value = "#{letsBuyApp}")
    private LetsBuyApp app;
    //This method is required for dependency injection (ManagedProperty)

    public void setApp(LetsBuyApp appBean) {
        this.app = appBean;
    }

    public void refreshFabrics() {
        this.fabrics = app.getActiveFabrics();
        customersForFabrics.clear();
        //resultMessage = new FacesMessage("Обновлено :"   ")
        FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "\u041e\u0431\u043d\u043e\u0432\u043b\u0435\u043d\u043e :", "");
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }
/*
    public void onRowEdit(RowEditEvent event) {
        //resultMessage = new FacesMessage("Сохранение :"   "Данные не сохранены.")
        FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "\u0421\u043e\u0445\u0440\u0430\u043d\u0435\u043d\u0438\u0435 :", "\u0414\u0430\u043d\u043d\u044b\u0435 \u043d\u0435 \u0441\u043e\u0445\u0440\u0430\u043d\u0435\u043d\u044b.");
        OrderItemModel mod = (OrderItemModel) event.getObject();
        boolean updated = app.updateOrderItemModel(mod);
        if (updated) {
            //resultMessage = new FacesMessage("Сохранение :"   "Данные сохранены успешно.")
            msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "\u0421\u043e\u0445\u0440\u0430\u043d\u0435\u043d\u0438\u0435 :", "\u0423\u0441\u043f\u0435\u0448\u043d\u043e\u0435 \u0437\u0430\u0432\u0435\u0440\u0448\u0435\u043d\u0438\u0435 \u043e\u043f\u0435\u0440\u0430\u0446\u0438\u0438."
                    + ((OrderItemModel) event.getObject()).getCode());
        }
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    public void onRowCancel(RowEditEvent event) {
//        OrderItemModel mod = app.getOrderItemModelById(((OrderItemModel) event.getObject()).getId());

        FacesMessage msg = new FacesMessage("Edit Cancelled", ((FabricModel) event.getObject()).getCode());
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }
*/
    public List<OrderItemModel> customers() {
         LOG.debug("begin customers()");
        return customers;
    }

            
    public List<OrderItemModel> customers(int fabricId) {
        LOG.debug("begin customers("+  fabricId +"), count="+(++count));
        StackTraceElement[] s = new Throwable().getStackTrace();
 //       long time = System.nanoTime();
        if(s[15].getMethodName().equals("encodeTbody")){
            // вызов по нажатию треугольника
           this.customers = app.getCustomersByFabricId(fabricId);
           customersForFabrics.put(fabricId, customers);
        }else{
            this.customers = customersForFabrics.get(fabricId);
            if(customers == null)
                customers = Collections.EMPTY_LIST;
        }
 //       LOG.debug("end customers("+  fabricId +"), time="+(System.nanoTime()-time)*1e-6);
//            LOG.debug("caller14: "+s[14].getMethodName()
//                    +"; caller15: "+s[15].getMethodName()
//                    +"; caller16: "+s[16].getMethodName()
//                    +"; caller17: "+s[17].getMethodName()
//                    +"; caller18: "+s[18].getMethodName()
//                    +"; caller19: "+s[19].getMethodName()
//                    );
        return customers;
    }

    public final List<OrderItemModel> getSelectedCustomers(){
         LOG.debug("begin getSelectedCustomers()");
        return customers(selectedFabricId);
    }
    
    public int getSelectedFabricId() {
         LOG.debug("begin getSelectedFabricId()");
        return selectedFabricId;
    }

    public void setSelectedFabricId(int selectedFabric) {
         LOG.debug("begin setSelectedFabricId()");
        this.selectedFabricId = selectedFabric;
    }
    
   public List<FabricModel> getFabrics() {
         LOG.debug("begin getFabrics()");
       if (fabrics == null) {
            fabrics = app.getActiveFabrics();
        }
        return fabrics;
    }

    public void updateOrderItem(int id) {
        LOG.debug("begin updateOrderItem(), id="+id+",  selectedFabricId="+selectedFabricId);
        updateOrderItem();
    }
    
    public void updateOrderItem() {
        LOG.debug("begin updateOrderItem()");
        int i = selectedFabricId;
        //resultMessage = new FacesMessage("Сохранение :"   "Данные не сохранены.")
        FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "\u0421\u043e\u0445\u0440\u0430\u043d\u0435\u043d\u0438\u0435 :", "\u0414\u0430\u043d\u043d\u044b\u0435 \u043d\u0435 \u0441\u043e\u0445\u0440\u0430\u043d\u0435\u043d\u044b.");
        int fabricId = selectedFabricItem.isWaitingList()?  -1 : selectedFabricId;
        selectedFabricItem.setFabricId(fabricId);
        boolean updated = app.updateOrderItemModel(selectedFabricItem );
        if (updated) {
            //resultMessage = new FacesMessage("Сохранение :"   "Данные сохранены успешно.")
//            msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "\u0421\u043e\u0445\u0440\u0430\u043d\u0435\u043d\u0438\u0435 :", "\u0423\u0441\u043f\u0435\u0448\u043d\u043e\u0435 \u0437\u0430\u0432\u0435\u0440\u0448\u0435\u043d\u0438\u0435 \u043e\u043f\u0435\u0440\u0430\u0446\u0438\u0438.");
            refreshFabrics();
        }
        FacesContext.getCurrentInstance().addMessage(null, msg);
}

    public OrderItemModel getSelectedFabricItem() {
        LOG.debug("begin getSelectedFabricItem");
        return selectedFabricItem;
    }

    public void setSelectedFabricItem(OrderItemModel selectedFabricItem) {
        LOG.debug("begin setSelectedFabricItem(OrderItemModel)");
        this.selectedFabricItem = selectedFabricItem;
    }

    /*  
    Метод формирует данные для показа пункта заказа клиента по заданному fabricId.
    Вызывается из action (если не используется PropertyActionListener)
    */
    public void showOrderItem(int orderItemId, int fabricId){
        LOG.debug("begin showOrderItem fabricId="+fabricId+"  orderItemId="+orderItemId);
        OrderItemModel mod = app.getOrderItemById(orderItemId);
        setSelectedFabricItem(mod);
        setSelectedFabricId(fabricId);
        LOG.debug("end showOrderItem fabricId="+fabricId+"  "+mod.toString());
    } 

}
