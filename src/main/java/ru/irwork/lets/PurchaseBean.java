package ru.irwork.lets;

import java.io.Serializable;
import java.util.*;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import org.apache.log4j.Logger;
import org.primefaces.event.RowEditEvent;
import ru.irwork.lets.model.PurchaseModel;

/**
 *
 * @author irwork
 */
@ManagedBean
@SessionScoped
public class PurchaseBean implements Serializable {

    private static final long serialVersionUID = 1L;
    private static final Logger LOG = Logger.getLogger(CommonFabricsBean.class);

    private PurchaseModel currentPurchase; // хранит название текущего марафона. используется на страницах shiva, index
    private String page; //  Default include.

    private String newPurchase;
    private PurchaseModel selectedPurchase;
    private List<PurchaseModel> purchases;

    @ManagedProperty(value = "#{letsBuyApp}")
    private LetsBuyApp app;
    //This method is required for dependency injection (ManagedProperty)

    public void setApp(LetsBuyApp appBean) {
        this.app = appBean;
    }

    @PostConstruct
    public void init() {
        this.page = "fabrics_input";
    }

    public String getNewPurchase() {
        return newPurchase;
    }

    public void setNewPurchase(String newPurchase) {
        this.newPurchase = newPurchase;
    }

    public String getPage() {
        return page;
    }

    public void setPage(String page) {
        this.page = page;
    }

    public PurchaseModel getCurrentPurchase() {
        if (currentPurchase == null) {
            currentPurchase = app.getCurrentPurchase();
        }
        return currentPurchase;
    }

    public void setCurrentPurchase(PurchaseModel currentPurchase) {
        this.currentPurchase = currentPurchase;
    }

    public void chooseCurrentPurchase() {
        LOG.debug("begin chooseCurrentPurchase()");
        if (selectedPurchase == null) {
//        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Марафон не выбран.", null);
            FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "\u041C\u0430\u0440\u0430\u0444\u043E\u043D\u0020\u043D\u0435\u0020\u0432\u044B\u0431\u0440\u0430\u043D.", null);
            FacesContext.getCurrentInstance().addMessage("Successful", message);
            return;
        }
        boolean choosed = app.setCurrentPurchase(selectedPurchase.getId());
        if (choosed) {
            this.currentPurchase = selectedPurchase;
        }

//        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Текущий марафон изменен.", null);
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "\u0422\u0435\u043A\u0443\u0449\u0438\u0439\u0020\u043C\u0430\u0440\u0430\u0444\u043E\u043D\u0020\u0438\u0437\u043C\u0435\u043D\u0435\u043D.", null);
        FacesContext.getCurrentInstance().addMessage("Successful", message);
    }

    public void createPurchase() {
        LOG.debug("begin createPurchase()");
        FacesMessage resultMessage = null;
        if (newPurchase == null || newPurchase.length() == 0) {
            //resultMessage = new FacesMessage("Некорректный формат ввода.");
            resultMessage = new FacesMessage(FacesMessage.SEVERITY_INFO, "\u041d\u0435\u043a\u043e\u0440\u0440\u0435\u043a\u0442\u043d\u044b\u0439 \u0444\u043e\u0440\u043c\u0430\u0442 \u0432\u0432\u043e\u0434\u0430.", null);
            FacesContext.getCurrentInstance().addMessage(null, resultMessage);
            return;
        }
        boolean saved = app.savePurchase(newPurchase);
        if (saved) {
            //resultMessage = new FacesMessage("Новый марафон создан.");
            resultMessage = new FacesMessage(FacesMessage.SEVERITY_INFO, "\u041D\u043E\u0432\u044B\u0439\u0020\u043C\u0430\u0440\u0430\u0444\u043E\u043D\u0020\u0441\u043E\u0437\u0434\u0430\u043D.", null);
            setNewPurchase(null);
            refreshPurchases();
        } else {
            //resultMessage = new FacesMessage("Новый марафон не создан.");
            resultMessage = new FacesMessage(FacesMessage.SEVERITY_INFO, "\u041D\u043E\u0432\u044B\u0439\u0020\u043C\u0430\u0440\u0430\u0444\u043E\u043D\u0020\u043D\u0435\u0020\u0441\u043E\u0437\u0434\u0430\u043D.", null);
        }
        FacesContext.getCurrentInstance().addMessage(null, resultMessage);
        LOG.debug("end createPurchase()");

    }

    /*   
    public void updatePurchase(){
       LOG.debug("begin updatePurchase()");
        FacesMessage resultMessage = null;
        boolean saved = app.updatePurchase(selectedPurchase);
        if (saved) {
            //resultMessage = new FacesMessage("Create new purchase.");
            resultMessage = new FacesMessage("Create new purchase.");
            setNewPurchase(null);
        } else {
            //resultMessage = new FacesMessage("bad.");
            resultMessage = new FacesMessage("Bad.");
        }
        FacesContext.getCurrentInstance().addMessage(null, resultMessage);
        LOG.debug("end updatePurchase()");
    }
     */
    public List<PurchaseModel> getView() {
        LOG.debug("begin getView()");
        if (this.purchases == null) {
            this.purchases = app.getPurchases();
        }
        return purchases;
    }

    public void refreshPurchases() {
        this.purchases = app.getPurchases();
    }

    public PurchaseModel getSelectedPurchase() {
        return selectedPurchase;
    }

    public void setSelectedPurchase(PurchaseModel selectedPurchase) {
        this.selectedPurchase = selectedPurchase;
    }

    public void onRowEdit(RowEditEvent event) {
        //resultMessage = new FacesMessage("Сохранение :"   "Данные не сохранены.")
        FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "\u0421\u043e\u0445\u0440\u0430\u043d\u0435\u043d\u0438\u0435 :", "\u0414\u0430\u043d\u043d\u044b\u0435 \u043d\u0435 \u0441\u043e\u0445\u0440\u0430\u043d\u0435\u043d\u044b.");
        PurchaseModel mod = (PurchaseModel) event.getObject();
        boolean updated = app.updatePurchase(mod);
        if (updated) {
            //resultMessage = new FacesMessage("Сохранение :"   "Данные сохранены успешно.")
            msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "\u0421\u043e\u0445\u0440\u0430\u043d\u0435\u043d\u0438\u0435 :", "\u0423\u0441\u043f\u0435\u0448\u043d\u043e\u0435 \u0437\u0430\u0432\u0435\u0440\u0448\u0435\u043d\u0438\u0435 \u043e\u043f\u0435\u0440\u0430\u0446\u0438\u0438."
                    + ((PurchaseModel) event.getObject()).getDescription());
        }
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    public void onRowCancel(RowEditEvent event) {
        FacesMessage msg = new FacesMessage("Edit Cancelled", ((PurchaseModel) event.getObject()).getDescription());
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    public List<PurchaseModel> getPurchases() {
        return purchases;
    }

    public void setPurchases(List<PurchaseModel> purchases) {
        this.purchases = purchases;
    }

}
